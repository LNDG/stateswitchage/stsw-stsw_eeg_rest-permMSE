%% Plot difference between methods

    pn.root = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/rest/B_analyses/E_permMSE/';

    addpath([pn.root, 'T_tools/fieldtrip-20170904/']); ft_defaults;

    pn.dataPath = [pn.root, 'B_data/C_MSE_Output_v1/'];

    load([pn.root, 'B_data/D_mpeMerged.mat'], 'mpeMerged', 'IDs')

%% calculate CBPA for all variants

    methodLabels = {'MSEVanilla', 'MSElp', 'MSEhp', 'MSEbp'};

%% CBPA: within-group: EC vs. EO

    % prepare_neighbours determines what sensors may form clusters
    cfg_neighb.method       = 'template';
    cfg_neighb.template     = 'elec1010_neighb.mat';
    cfg_neighb.channel      = mpeMerged{1,1}.label;

    cfgStat = [];
    cfgStat.channel          = 'all';
    cfgStat.method           = 'montecarlo';
    cfgStat.statistic        = 'ft_statfun_depsamplesT';
    cfgStat.correctm         = 'cluster';
    cfgStat.clusteralpha     = 0.05;
    cfgStat.clusterstatistic = 'maxsum';
    cfgStat.minnbchan        = 3;
    cfgStat.tail             = 0;
    cfgStat.clustertail      = 0;
    cfgStat.alpha            = 0.025;
    cfgStat.numrandomization = 1000;
    cfgStat.neighbours       = ft_prepare_neighbours(cfg_neighb, mpeMerged{1,1});

    subj = size(mpeMerged{1,1}.MSEVanilla,1);
    conds = 2;
    design = zeros(2,conds*subj);
    for indCond = 1:conds
    for i = 1:subj
        design(1,(indCond-1)*subj+i) = indCond;
        design(2,(indCond-1)*subj+i) = i;
    end
    end
    cfgStat.design   = design;
    cfgStat.ivar     = 1;
    cfgStat.uvar     = 2;

    for indMethod = 1:numel(methodLabels)
        cfgStat.parameter = methodLabels{indMethod};
        [stat{1,indMethod}] = ft_freqstatistics(cfgStat, mpeMerged{1,1}, mpeMerged{1,2});
    end

    subj = size(mpeMerged{2,1}.MSEVanilla,1);
    conds = 2;
    design = zeros(2,conds*subj);
    for indCond = 1:conds
    for i = 1:subj
        design(1,(indCond-1)*subj+i) = indCond;
        design(2,(indCond-1)*subj+i) = i;
    end
    end
    cfgStat.design   = design;
    cfgStat.ivar     = 1;
    cfgStat.uvar     = 2;

    for indMethod = 1:numel(methodLabels)
        cfgStat.parameter = methodLabels{indMethod};
        [stat{2,indMethod}] = ft_freqstatistics(cfgStat, mpeMerged{2,1}, mpeMerged{2,2});
    end

%% CBPA: between-group: EC: OA vs. YA; EO: OA vs. YA

    cfgStat = [];
    cfgStat.channel          = 'all';
    cfgStat.method           = 'montecarlo';
    cfgStat.statistic        = 'ft_statfun_indepsamplesT';
    cfgStat.correctm         = 'cluster';
    cfgStat.clusteralpha     = 0.05;
    cfgStat.clusterstatistic = 'maxsum';
    cfgStat.minnbchan        = 3;
    cfgStat.tail             = 0;
    cfgStat.clustertail      = 0;
    cfgStat.alpha            = 0.025;
    cfgStat.numrandomization = 1000;
    cfgStat.neighbours       = ft_prepare_neighbours(cfg_neighb, mpeMerged{1,1});

    N_1 = size(mpeMerged{1,1}.MSEVanilla,1);
    N_2 = size(mpeMerged{2,1}.MSEVanilla,1);
    cfgStat.design = zeros(1,N_1+N_2);
    cfgStat.design(1,1:N_1) = 1;
    cfgStat.design(1,N_1+1:end) = 2;

    for indMethod = 1:numel(methodLabels)
        cfgStat.parameter = methodLabels{indMethod};
        [stat{3,indMethod}] = ft_freqstatistics(cfgStat, mpeMerged{2,1}, mpeMerged{1,1});
        [stat{4,indMethod}] = ft_freqstatistics(cfgStat, mpeMerged{2,2}, mpeMerged{1,2});
    end

%% save statistics output
    
    save([pn.root, 'B_data/C_CBPA_Age_Condition.mat'], 'stat', 'methodLabels')