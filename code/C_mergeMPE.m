%% merge MPE and R estimates for all versions
 
    pn.dataPath = '/Users/kosciessa/Desktop/mntTardis/STSWD_Rest/WIP_eeg/E_permMSE/B_data/C_MSE_Output_v1/';

    IDs{1} = {'1117';'1118';'1120';'1124';'1126';'1131';'1132';'1135';'1136';'1138';...
        '1144';'1151';'1158';'1160';'1163';'1164';'1167';'1169';'1172';'1173';...
        '1178';'1182';'1215';'1216';'1219';'1221';'1223';'1227';'1228';...
        '1233';'1234';'1237';'1239';'1240';'1243';'1245';'1247';'1250';'1252';...
        '1257';'1261';'1265';'1266';'1268';'1270';'1276';'1281'};


    IDs{2} = {'2104';'2107';'2108';'2112';'2118';'2120';'2121';'2123';'2125';'2129';...
        '2130';'2131';'2132';'2133';'2134';'2135';'2139';'2140';'2145';'2147';...
        '2149';'2157';'2160';'2202';'2203';'2205';'2206';'2209';'2210';...
        '2211';'2213';'2214';'2215';'2216';'2217';'2219';'2222';'2224';'2226';...
        '2227';'2236';'2237';'2238';'2241';'2244';'2246';'2248';'2250';'2251';...
        '2252';'2258';'2261'};

    conds = {'EC'; 'EO'};

    for indGroup = 1:2
        for indID = 1:numel(IDs{indGroup})
            for indCond = 1:2
                try
                curID = IDs{indGroup}{indID};
                load([pn.dataPath, curID, '_',conds{indCond},'_MSE_OUT_no_pointavg.mat']);
                mpeMerged{indGroup,indCond}.MSEVanilla(indID,:,:) = mse.permen(1:60,:);
                load([pn.dataPath, curID, '_',conds{indCond},'_MSE_OUT_lp_filtskip.mat']);
                mpeMerged{indGroup,indCond}.MSElp(indID,:,:) = mse.permen(1:60,:);
                load([pn.dataPath, curID, '_',conds{indCond},'_MSE_OUT_hp_filtskip_v2.mat']);
                mpeMerged{indGroup,indCond}.MSEhp(indID,:,:) = mse.permen(1:60,:);
                load([pn.dataPath, curID, '_',conds{indCond},'_MSE_OUT_bp_filtskip.mat']);
                mpeMerged{indGroup,indCond}.MSEbp(indID,:,:) = mse.permen(1:60,:);
                % add info
                mpeMerged{indGroup,indCond}.dimord = 'subj_chan_freq';
                mpeMerged{indGroup,indCond}.label = mse.label(1:60);
                mpeMerged{indGroup,indCond}.timescales = mse.timescales;
                mpeMerged{indGroup,indCond}.freq = (1./[1:42])*250;
                mpeMerged{indGroup,indCond}.time = mse.time;
                catch
                    disp([curID, ' ',conds{indCond}, ' skipped']);
                    continue
                end
            end
        end
    end

    save('/Users/kosciessa/Desktop/mntTardis/STSWD_Rest/WIP_eeg/E_permMSE/B_data/D_mpeMerged.mat', 'mpeMerged', 'IDs')