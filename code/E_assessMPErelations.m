%% Plot difference between methods

    pn.root = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/rest/B_analyses/E_permMSE/';
    addpath([pn.root, 'T_tools/fieldtrip-20170904/']); ft_defaults;
    pn.dataPath = [pn.root, 'B_data/C_MSE_Output_v1/'];
    load([pn.root, 'B_data/D_mpeMerged.mat'], 'mpeMerged', 'IDs')
    pn.shadedError = ['/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/rest/B_analyses/A_MSE_CSD_multiVariant/T_tools/shadedErrorBar']; addpath(pn.shadedError);

%% Supplement: correlation between spectral slope and entropy: scale 1 and 5-10 for all methods

    labels = {'MSEVanilla'; 'MSElp'; 'MSEhp'; 'MSEbp'};
    scales = {[1]; [1:10]};

    labelTitle = {'Vanilla'; 'Low-pass'; 'High-pass'; 'Band-pass'};
    scaleTitle = {'1'; '5-10'};

    h = figure('units','normalized','position',[.1 .1 .7 .4]);
    for indLabel = 1:4
        for indScale = 1:2
            subplot(2,4,(indScale-1)*4+indLabel); cla; hold on;
            x = squeeze(nanmedian(nanmean(mpeMerged{1,indCond}.(labels{indLabel})(:,channels,scales{indScale}),3),2));
            y = squeeze(nanmedian(linFit_2_30_EO(idx_YA_fft, channels),2));
            l1 = scatter(x,y, 'filled', 'k'); lsline;
            [rl1,pl1] = corrcoef(x,y);
            % add OA
            x = squeeze(nanmedian(nanmean(mpeMerged{2,indCond}.(labels{indLabel})(:,channels,scales{indScale}),3),2));
            y = squeeze(nanmedian(linFit_2_30_EO(idx_OA_fft, channels),2));
            l2 = scatter(x,y, 'filled', 'r'); lsline;
            [rl2,pl2] = corrcoef(x,y);
            legend([l1, l2], {['Younger Adults: r=', num2str(round(rl1(2),2))], ...
                ['Older Adults: r=', num2str(round(rl2(2),2))]}, 'Location', 'NorthWest'); legend('boxoff');
            xlabel(['Vanilla MPE, Scale ', scaleTitle{indScale}]); ylabel('PSD Slope 2-64 Hz (excl. 7-13 Hz)');
            title(['Fine-scale permutation entropy: ', labelTitle{indLabel}])
        end
    end

%% Supplement: plot correlations by scale

    h = figure('units','normalized','position',[.1 .1 .7 .4]);
    for indLabel = 1:4
        for indScale = 1:2
            subplot(2,4,(indScale-1)*4+indLabel); cla; hold on;
                for indScale = 1:41
                    cla; hold on;
                    x = squeeze(nanmedian(nanmean(mpeMerged{1,indCond}.(labels{indLabel})(:,channels,indScale),3),2));
                    y = squeeze(nanmedian(linFit_2_30_EO(idx_YA_fft, channels),2));
                    [rl1,pl1] = corrcoef(x,y);
                    Rmat(1,indScale) = rl1(2);
                    Pmat(1,indScale) = pl1(2);
                    % add OA
                    x = squeeze(nanmedian(nanmean(mpeMerged{2,indCond}.(labels{indLabel})(:,channels,indScale),3),2));
                    y = squeeze(nanmedian(linFit_2_30_EO(idx_OA_fft, channels),2));
                    [rl2,pl2] = corrcoef(x,y);
                    Rmat(2,indScale) = rl2(2);
                    Pmat(2,indScale) = pl2(2);
                end
                hold on;plot(Rmat(1,:), 'LineWidth', 2); plot(Rmat(2,:), 'LineWidth', 2)
                xlim([1 41]); ylim([-1 1])
                set(gca, 'XScale', 'log')
                scales = 1:41;
                set(gca, 'XTick',round(logspace(log10(1), log10(41), 8)));
                tmp_meas = [round(mpeMerged{1,indCond}.freq(get(gca, 'XTick')),0); round(scales(get(gca, 'XTick')),0)];
                xLabels = [];
                for indScale = 1:size(tmp_meas,2)
                    xLabels{indScale} = [num2str(tmp_meas(1,indScale)), ' (',num2str(tmp_meas(2,indScale)),')'];
                end; clear tmp_meas;
                set(gca, 'XTickLabels', xLabels);
    %             set(gca, 'XDir','reverse')
        end
    end

%% Supplement: show PSD/R with errorbars

    indCond = 2; channels = 51:60;

    h = figure('units','normalized','position',[0 0 .7 .4]);
    subplot(1,4,1); hold on;
        curAverage = nanmean(mpeMerged{1,indCond}.MSEVanilla(:, channels,:),2);
        standError = nanstd(curAverage,1)./sqrt(size(curAverage,1));
        l1 = shadedErrorBar([],nanmean(curAverage,1),standError, 'lineprops', 'r', 'patchSaturation', .05);
        curAverage = nanmean(mpeMerged{2,indCond}.MSEVanilla(:, channels,:),2);
        standError = nanstd(curAverage,1)./sqrt(size(curAverage,1));
        l2 = shadedErrorBar([],nanmean(curAverage,1),standError, 'lineprops', 'k', 'patchSaturation', .05);
        xlim([1 41]); ylim([0 1.3]); title('Vanilla (r = .5, m = 2)');
        set(gca, 'XTick', 1:6:41); set(gca, 'XTickLabel', round(mpeMerged{1,indCond}.freq(1:6:41))); xlabel('Scale [Hz]'); ylabel('Sample Entropy');
        legend([l1.mainLine, l2.mainLine],{'Young adults'; 'Older adults'}); legend('boxoff');
    subplot(1,4,2); hold on;
        curAverage = nanmean(mpeMerged{1,indCond}.MSElp(:, channels,:),2);
        standError = nanstd(curAverage,1)./sqrt(size(curAverage,1));
        l1 = shadedErrorBar([],nanmean(curAverage,1),standError, 'lineprops', 'r', 'patchSaturation', .05);
        curAverage = nanmean(mpeMerged{2,indCond}.MSElp(:, channels,:),2);
        standError = nanstd(curAverage,1)./sqrt(size(curAverage,1));
        l2 = shadedErrorBar([],nanmean(curAverage,1),standError, 'lineprops', 'k', 'patchSaturation', .05);
        xlim([1 41]); ylim([0 1.3]); title('Lowpass + scale-wise R');
        set(gca, 'XTick', 1:6:41); set(gca, 'XTickLabel', round(mpeMerged{1,indCond}.freq(1:6:41))); xlabel('Scale [Hz]'); ylabel('Sample Entropy');
    subplot(1,4,3); hold on;
        curAverage = nanmean(mpeMerged{1,indCond}.MSEhp(:, channels,:),2);
        standError = nanstd(curAverage,1)./sqrt(size(curAverage,1));
        l1 = shadedErrorBar([],nanmean(curAverage,1),standError, 'lineprops', 'r', 'patchSaturation', .05);
        curAverage = nanmean(mpeMerged{2,indCond}.MSEhp(:, channels,:),2);
        standError = nanstd(curAverage,1)./sqrt(size(curAverage,1));
        l2 = shadedErrorBar([],nanmean(curAverage,1),standError, 'lineprops', 'k', 'patchSaturation', .05);
        xlim([1 41]); ylim([0 1.3]); title('Highpass + scale-wise R');
        set(gca, 'XTick', 1:6:41); set(gca, 'XTickLabel', round(mpeMerged{1,indCond}.freq(1:6:41))); xlabel('Scale [Hz]'); ylabel('Sample Entropy');
    subplot(1,4,4); hold on;
        curAverage = nanmean(mpeMerged{1,indCond}.MSEbp(:, channels,:),2);
        standError = nanstd(curAverage,1)./sqrt(size(curAverage,1));
        l1 = shadedErrorBar([],nanmean(curAverage,1),standError, 'lineprops', 'r', 'patchSaturation', .05);
        curAverage = nanmean(mpeMerged{2,indCond}.MSEbp(:, channels,:),2);
        standError = nanstd(curAverage,1)./sqrt(size(curAverage,1));
        l2 = shadedErrorBar([],nanmean(curAverage,1),standError, 'lineprops', 'k', 'patchSaturation', .05);
        xlim([1 41]); ylim([0 1.3]); title('Bandpass + scale-wise R');
        set(gca, 'XTick', 1:6:41); set(gca, 'XTickLabel', round(mpeMerged{1,indCond}.freq(1:6:41))); xlabel('Scale [Hz]'); ylabel('Sample Entropy');
    set(findall(gcf,'-property','FontSize'),'FontSize',18)
    
%% Supplement: intra-individual scale-wise difference in Vanilla MSE is associated with slopes
    
    h = figure('units','normalized','position',[.1 .1 .7 .2]);
     subplot(1,4,1); cla; hold on;
        x = squeeze(nanmedian(nanmean(mpeMerged{1,indCond}.MSEVanilla(:,channels,1:10),3),2));
        y = squeeze(nanmedian(linFit_2_30_EO(idx_YA_fft, channels),2));
        l1 = scatter(x,y, 'filled', 'k'); lsline;
        [rl1,pl1] = corrcoef(x,y);
        % add OA
        x = squeeze(nanmedian(nanmean(mpeMerged{2,indCond}.MSEVanilla(:,channels,1:10),3),2));
        y = squeeze(nanmedian(linFit_2_30_EO(idx_OA_fft, channels),2));
        l2 = scatter(x,y, 'filled', 'r'); lsline;
        [rl2,pl2] = corrcoef(x,y);
        legend([l1, l2], {['Younger Adults: r=', num2str(round(rl1(2),2))], ...
            ['Older Adults: r=', num2str(round(rl2(2),2))]}, 'Location', 'NorthWest'); legend('boxoff');
        xlabel('Vanilla MPE, Scales 1:10'); ylabel('PSD Slope 2-64 Hz (excl. 7-13 Hz)');
        title({'Fine-scale Vanilla MPE ';'positively relates to PSD slope'})

     subplot(1,4,2); cla; hold on;
        x = squeeze(nanmedian(nanmean(mpeMerged{1,indCond}.MSEbp(:,channels,1:10),3),2));
        y = squeeze(nanmedian(linFit_2_30_EO(idx_YA_fft, channels),2));
        l1 = scatter(x,y, 'filled', 'k'); lsline;
        [rl1,pl1] = corrcoef(x,y);
        % add OA
        x = squeeze(nanmedian(nanmean(mpeMerged{2,indCond}.MSEbp(:,channels,1:10),3),2));
        y = squeeze(nanmedian(linFit_2_30_EO(idx_OA_fft, channels),2));
        l2 = scatter(x,y, 'filled', 'r'); lsline;
        [rl2,pl2] = corrcoef(x,y);
        legend([l1, l2], {['Younger Adults: r=', num2str(round(rl1(2),2))], ...
            ['Older Adults: r=', num2str(round(rl2(2),2))]}, 'Location', 'SouthWest'); legend('boxoff');
        xlabel('Bandpass MPE, Scales 1:10'); ylabel('PSD Slope 2-64 Hz (excl. 7-13 Hz)');
        title({'Fine-scale Bandpass MPE ';'negatively relates to PSD slope'})

     subplot(1,4,3); cla; hold on;
        x = squeeze(nanmedian(nanmean(mpeMerged{1,indCond}.MSEbp(:,channels,1:10),3),2));
        y = squeeze(nanmedian(nanmean(mpeMerged{1,indCond}.MSEVanilla(:,channels,1:10),3),2));
        l1 = scatter(x,y, 'filled', 'k'); lsline;
        [rl1,pl1] = corrcoef(x,y);
        % add OA
        x = squeeze(nanmedian(nanmean(mpeMerged{2,indCond}.MSEbp(:,channels,1:10),3),2));
        y = squeeze(nanmedian(nanmean(mpeMerged{2,indCond}.MSEVanilla(:,channels,1:10),3),2));
        l2 = scatter(x,y, 'filled', 'r'); lsline;
        [rl2,pl2] = corrcoef(x,y);
        legend([l1, l2], {['Younger Adults: r=', num2str(round(rl1(2),2))], ...
            ['Older Adults: r=', num2str(round(rl2(2),2))]}, 'Location', 'SouthWest'); legend('boxoff');
        xlabel('Bandpass MPE, Scales 1:10'); ylabel('Vanilla MPE, Scales 1:10')
        title({'PE is anticorrelated between ';'Vanilla and bandpass'})

     subplot(1,4,4); cla; hold on;
        x = squeeze(nanmedian(nanmean(mpeMerged{1,indCond}.MSEbp(:,channels,1:10),3),2))-...
            squeeze(nanmedian(nanmean(mpeMerged{1,indCond}.MSEVanilla(:,channels,1:10),3),2));
            y = squeeze(nanmedian(linFit_2_30_EO(idx_YA_fft, channels),2));

        l1 = scatter(x,y, 'filled', 'k'); lsline;
        [rl1,pl1] = corrcoef(x,y);
        % add OA
        x = squeeze(nanmedian(nanmean(mpeMerged{2,indCond}.MSEbp(:,channels,1:10),3),2))-...
            squeeze(nanmedian(nanmean(mpeMerged{2,indCond}.MSEVanilla(:,channels,1:10),3),2));
        y = squeeze(nanmedian(linFit_2_30_EO(idx_OA_fft, channels),2));
        l2 = scatter(x,y, 'filled', 'r'); lsline;
        [rl2,pl2] = corrcoef(x,y);
        legend([l1, l2], {['Younger Adults: r=', num2str(round(rl1(2),2))], ...
            ['Older Adults: r=', num2str(round(rl2(2),2))]}, 'Location', 'SouthWest'); legend('boxoff');
        xlabel('Bandpass-Vanilla MPE, Scales 1:10'); ylabel('PSD Slope 2-64 Hz (excl. 7-13 Hz)');
        title({'Individual shift in PE is';'associated with spectral slopes'})

        set(findall(gcf,'-property','FontSize'),'FontSize',14)

        pn.plotFolder = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/rest/B_analyses/A_MSE_CSD_multiVariant/C_figures/';
        figureName = 'D_MPE_InversionVanillaBP_PSDslope';
        saveas(h, [pn.plotFolder, figureName], 'fig');
        saveas(h, [pn.plotFolder, figureName], 'epsc');
        saveas(h, [pn.plotFolder, figureName], 'png');
