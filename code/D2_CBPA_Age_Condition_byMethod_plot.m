%% plot results of MPE CBPA

    pn.root = '/Users/kosciessa/Desktop/mntTardis/STSWD_Rest/WIP_eeg/E_permMSE/';

    addpath([pn.root, 'T_tools/fieldtrip-20170904']); ft_defaults;

    load([pn.root, 'B_data/C_CBPA_Age_Condition.mat'], 'stat', 'methodLabels')

    % add convertPtoExponential
    addpath('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/rest/B_analyses/A_MSE_CSD_multiVariant/T_tools/')
    addpath('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/rest/B_analyses/A_MSE_CSD_multiVariant/T_tools/brewermap')
    addpath('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/rest/B_analyses/A_MSE_CSD_multiVariant/T_tools/altmany-export_fig-4282d74')

    contrastLabels = {'YA: EC vs. EO'; 'OA: EC vs. EO'; 'EC: OA vs. YA'; 'EO: OA vs. YA'};

    % 1 - YA: EC vs. EO
    % 2 - OA: EC vs. EO
    % 3 - EC: YA vs. OA
    % 4 - EO: YA vs. OA

    % X, 1... - method in methodLabels

    h = figure('units','normalized','position',[0 0 1 1]);
    for indContrast = 1:4
        for indMethod = 1:numel(methodLabels)
            subplot(numel(methodLabels),4,(indMethod-1)*4+indContrast);
            imagesc(stat{indContrast,indMethod}.stat, 'AlphaData', .5); 
            caxis([-6 6]) 
            hold on;
            imagesc(stat{indContrast,indMethod}.stat, 'AlphaData', stat{indContrast,indMethod}.mask); 
            set(gca, 'XTick', 1:4:41); set(gca, 'XTickLabel', round(stat{indContrast,indMethod}.freq(1:4:41))); xlabel('Scale [Hz]'); ylabel('Channel');
            title([contrastLabels{indContrast}, ';', methodLabels{indMethod}])
        end
    end
    cBrew = brewermap(500,'RdBu');
    cBrew = flipud(cBrew);
    colormap(cBrew)
    set(findall(gcf,'-property','FontSize'),'FontSize',18)
    pn.plotFolder = [pn.root, 'C_figures/'];
    figureName = 'E3_statsWithMasking';
    % saveas(h, [pn.plotFolder, figureName], 'fig');
    % saveas(h, [pn.plotFolder, figureName], 'epsc');
    saveas(h, [pn.plotFolder, figureName], 'png');

%% plot only EO: OA vs. YA

    indContrast = 4;
    labels = {'MPE: Vanilla'; 'MPE: Lowpass'; 'MPE: Highpass'; 'MPE: Bandpass'};

    h = figure('units','normalized','position',[0 0 .8 .4]);
    for indMethod = 2:numel(methodLabels)
        subplot(1,3,indMethod-1);
        imagesc(stat{indContrast,indMethod}.stat, 'AlphaData', .5); 
        caxis([-6 6]) 
        hold on;
        imagesc(stat{indContrast,indMethod}.stat, 'AlphaData', stat{indContrast,indMethod}.mask); 
        set(gca, 'XTick', [1:7:38, 42]);
        set(gca, 'XTickLabels', round(stat{1,1}.freq(get(gca, 'XTick')),0));
        set(gca, 'XDir', 'reverse')
        xlabel('Frequency (Hz)'); ylabel({'Channel: anterior-posterior'});
        set(gca, 'YTick', []);
        cb = colorbar; set(get(cb,'label'),'string','t values');
        title(labels(indMethod))
    end
    set(findall(gcf,'-property','FontSize'),'FontSize',30)
    cBrew = brewermap(500,'RdBu');
    cBrew = flipud(cBrew);
    colormap(cBrew)
    pn.plotFolder = [pn.root, 'C_figures/'];
    figureName = 'E3_CBPA_EO_OAvsYA';
    export_fig([pn.plotFolder, figureName, '.pdf'], '-transparent')
    saveas(h, [pn.plotFolder, figureName], 'fig');
    saveas(h, [pn.plotFolder, figureName], 'png');

%% topography

    cfg = [];
    cfg.layout = 'acticap-64ch-standard2.mat';
    cfg.parameter = 'powspctrm';
    cfg.comment = 'no';
    cfg.colorbar = 'SouthOutside';
    cfg.zlim = [-5 5];

    h = figure('units','normalized','position',[.1 .1 .7 .3]);
    subplot(1,2,1)
        cfg.highlight = 'on';
        cfg.highlightchannel = stat{1,1}.label(max(stat{4,2}.mask(:,41),[],2));
        plotData = [];
        plotData.label = stat{1,1}.label; % {1 x N}
        plotData.dimord = 'chan';
        plotData.powspctrm = squeeze(nanmean(stat{4,2}.stat(:,41),2));
        ft_topoplotER(cfg,plotData);
        title('High-frequency low-pass entropy: OA>YA')
    subplot(1,2,2)
        cfg.highlight = 'on';
        cfg.highlightchannel = stat{1,1}.label(max(stat{4,3}.mask(:,1:5),[],2));
        plotData = [];
        plotData.label = stat{1,1}.label; % {1 x N}
        plotData.dimord = 'chan';
        plotData.powspctrm = squeeze(nanmean(stat{4,3}.stat(:,1:5),2));
        ft_topoplotER(cfg,plotData);
        title('Low-frequency high-pass entropy: OA>YA')
    set(findall(gcf,'-property','FontSize'),'FontSize',18)
    pn.plotFolder = [pn.root, 'C_figures/'];
    figureName = 'E3_TopoAgeDifferences';
    saveas(h, [pn.plotFolder, figureName], 'fig');
    saveas(h, [pn.plotFolder, figureName], 'epsc');
    saveas(h, [pn.plotFolder, figureName], 'png');

%% topography of bandpass MPE differences

    cBrew = brewermap(500,'RdBu');
    cBrew = flipud(cBrew);

    cfg = [];
    cfg.layout = 'acticap-64ch-standard2.mat';
    cfg.parameter = 'powspctrm';
    cfg.comment = 'no';
    cfg.colorbar = 'SouthOutside';
    cfg.style = 'both';
    cfg.colormap = cBrew;
    cfg.zlim = [-5 5];

    h = figure('units','normalized','position',[.1 .1 .7 .3]);
    subplot(1,2,1)
        cfg.highlight = 'on';
        cfg.highlightchannel = stat{1,1}.label(max(stat{4,4}.mask(:,15:20),[],2));
        plotData = [];
        plotData.label = stat{1,1}.label; % {1 x N}
        plotData.dimord = 'chan';
        plotData.powspctrm = squeeze(nanmedian(stat{4,4}.stat(:,15:20),2));
        ft_topoplotER(cfg,plotData);
        title('Alpha entropy OA vs. YA')
    subplot(1,2,2)
        cfg.highlight = 'on';
        cfg.highlightchannel = stat{1,1}.label(max(stat{4,4}.mask(:,23:27),[],2));
        plotData = [];
        plotData.label = stat{1,1}.label; % {1 x N}
        plotData.dimord = 'chan';
        plotData.powspctrm = squeeze(nanmedian(stat{4,4}.stat(:,23:27),2));
        ft_topoplotER(cfg,plotData);
        title('Beta entropy OA vs. YA')
    set(findall(gcf,'-property','FontSize'),'FontSize',25)
    pn.plotFolder = [pn.root, 'C_figures/'];
    figureName = 'E4_bandpassMSEdifferences';

    saveas(h, [pn.plotFolder, figureName], 'fig');
    saveas(h, [pn.plotFolder, figureName], 'epsc');
    saveas(h, [pn.plotFolder, figureName], 'png');


%% plot overview plot

    indContrast = 4;
    labels = {{'MPE: Vanilla'; 'OA vs. YA'}; {'MPE: Lowpass'; 'OA vs. YA'}; {'MPE: Highpass'; 'OA vs. YA'}; {'MPE: Bandpass'; 'OA vs. YA'}};

    h = figure('units','normalized','position',[0 0 .8 .4]);
    figIdx = {1,2,[3,4,5]};
    for indMethod = 2:numel(methodLabels)
        subplot(2,5,[figIdx{indMethod-1}]);
        imagesc(stat{indContrast,indMethod}.stat, 'AlphaData', .3); 
        caxis([-6 6]) 
        hold on;
        imagesc(stat{indContrast,indMethod}.stat, 'AlphaData', stat{indContrast,indMethod}.mask); 
        set(gca, 'XTick', [1:8:36, 42]);
        set(gca, 'XTickLabels', round(stat{1,1}.freq(get(gca, 'XTick')),0));
        set(gca, 'XDir', 'reverse')
        xlabel('Frequency (Hz)'); ylabel('Channel: ant.-post.');
        cb = colorbar; set(get(cb,'title'),'string','t values');
        title(labels{indMethod})
    end

    cBrew = brewermap(500,'RdBu');
    cBrew = flipud(cBrew);
    colormap(cBrew)

    cfg = [];
    cfg.layout = 'acticap-64ch-standard2.mat';
    cfg.parameter = 'powspctrm';
    cfg.comment = 'no';
    cfg.colorbar = 'EastOutside';
    cfg.style = 'both';
    cfg.colormap = cBrew;
    cfg.zlim = [-6 6];

    subplot(2,5,6)
        cfg.highlight = 'on';
        cfg.highlightchannel = stat{1,1}.label(max(stat{4,2}.mask(:,stat{4,4}.freq>15),[],2));
        plotData = [];
        plotData.label = stat{1,1}.label; % {1 x N}
        plotData.dimord = 'chan';
        plotData.powspctrm = squeeze(nanmean(stat{4,2}.stat(:,stat{4,4}.freq>15),2));
        ft_topoplotER(cfg,plotData);
        pval = []; pval = convertPtoExponential(stat{4,2}.posclusters(1).prob);
        title({'15 Hz+ low-pass entropy', ['OA vs. YA; p = ', pval{1}]});
        cb = colorbar; set(get(cb,'label'),'string','t values');
    subplot(2,5,7)
        cfg.highlight = 'on';
        cfg.highlightchannel = stat{1,1}.label(max(stat{4,3}.mask(:,stat{4,4}.freq<15),[],2));
        plotData = [];
        plotData.label = stat{1,1}.label; % {1 x N}
        plotData.dimord = 'chan';
        plotData.powspctrm = squeeze(nanmean(stat{4,3}.stat(:,stat{4,4}.freq<15),2));
        ft_topoplotER(cfg,plotData);
        pval = []; pval = convertPtoExponential(stat{4,3}.posclusters(1).prob);
        title({'15 Hz- high-pass entropy', ['OA vs. YA; p = ', pval{1}]});
        cb = colorbar; set(get(cb,'label'),'string','t values');
    subplot(2,5,9)
        cfg.highlight = 'on';
        cfg.highlightchannel = stat{1,1}.label(max(stat{4,4}.mask(:,stat{4,4}.freq>10 & stat{4,4}.freq<12),[],2));
        plotData = [];
        plotData.label = stat{1,1}.label; % {1 x N}
        plotData.dimord = 'chan';
        plotData.powspctrm = squeeze(nanmedian(stat{4,4}.stat(:,stat{4,4}.freq>10 & stat{4,4}.freq<12),2));
        ft_topoplotER(cfg,plotData);
        pval = []; pval = convertPtoExponential(stat{4,4}.posclusters(1).prob);
        title({'Alpha (10-12 Hz) bandpass entropy', ['OA vs. YA; p = ', pval{1}]});
        cb = colorbar; set(get(cb,'label'),'string','t values');
    subplot(2,5,8)
        cfg.highlight = 'on';
        cfg.highlightchannel = stat{1,1}.label(max(stat{4,4}.mask(:,stat{4,4}.freq>14 & stat{4,4}.freq<20),[],2));
        plotData = [];
        plotData.label = stat{1,1}.label; % {1 x N}
        plotData.dimord = 'chan';
        plotData.powspctrm = squeeze(nanmedian(stat{4,4}.stat(:,stat{4,4}.freq>14 & stat{4,4}.freq<20),2));
        ft_topoplotER(cfg,plotData);
        pval = []; pval = convertPtoExponential(stat{4,4}.negclusters(1).prob);
        title({'Beta (14-20 Hz) bandpass entropy', ['OA vs. YA; p = ', pval{1}]});
        cb = colorbar; set(get(cb,'label'),'string','t values');
    subplot(2,5,10)
        cfg.highlight = 'on';
        cfg.highlightchannel = stat{1,1}.label(max(stat{4,4}.mask(:,stat{4,4}.freq>7 & stat{4,4}.freq<8),[],2));
        plotData = [];
        plotData.label = stat{1,1}.label; % {1 x N}
        plotData.dimord = 'chan';
        plotData.powspctrm = squeeze(nanmedian(stat{4,4}.stat(:,stat{4,4}.freq>7 & stat{4,4}.freq<8),2));
        ft_topoplotER(cfg,plotData);
        pval = []; pval = convertPtoExponential(stat{4,4}.negclusters(2).prob);
        title({'Theta (7-8 Hz) bandpass entropy', ['OA vs. YA; p = ', pval{1}]});
        cb = colorbar; set(get(cb,'label'),'string','t values');

        set(findall(gcf,'-property','FontSize'),'FontSize',18)
    pn.plotFolder = [pn.root, 'C_figures/'];
    figureName = 'C2_overviewPlot_ageDifferences_MPE';

    saveas(h, [pn.plotFolder, figureName], 'fig');
    saveas(h, [pn.plotFolder, figureName], 'epsc');
    saveas(h, [pn.plotFolder, figureName], 'png');