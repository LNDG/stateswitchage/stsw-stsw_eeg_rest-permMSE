%% FIGURE S4 plot association of fine-scale Vanilla MPE to PSD slopes

    load('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/rest/B_analyses/E_permMSE/B_data/D_mpeMerged.mat', 'mpeMerged', 'IDs')
    load('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/rest/B_analyses/B_FFT/B_data/D_PSDslopes.mat')

    commonYA = info.IDs_all(ismember(info.IDs_all, IDs{1}));
    commonOA = info.IDs_all(ismember(info.IDs_all, IDs{2}));

    idx_YA_fft = ismember(info.IDs_all, commonYA);
    idx_OA_fft = ismember(info.IDs_all, commonOA);
    idx_YA_mse = ismember(IDs{1}, commonYA);
    idx_OA_mse = ismember(IDs{2}, commonOA);

    indCond = 2; channels = 51:60;

    h = figure('units','normalized','position',[.1 .1 .15 .25]);
        cla; hold on;
        x = squeeze(nanmedian(nanmean(mpeMerged{1,indCond}.MSEVanilla(idx_YA_mse,channels,1:3),3),2));
        y = squeeze(nanmedian(linFit_2_30_EO(idx_YA_fft, channels),2));
        l1 = scatter(x,y, 50, 'filled', 'k'); y1_ls = polyval(polyfit(x,y,1),x); y1_ls = plot(x, y1_ls, 'Color', 'r', 'LineWidth', 3);
        [rl1,pl1] = corrcoef(x,y);
        % add OA
        x = squeeze(nanmedian(nanmean(mpeMerged{2,indCond}.MSEVanilla(idx_OA_mse,channels,1:3),3),2));
        y = squeeze(nanmedian(linFit_2_30_EO(idx_OA_fft, channels),2));
        l2 = scatter(x,y, 50,'filled', 'r'); y2_ls = polyval(polyfit(x,y,1),x); y2_ls = plot(x, y2_ls, 'Color', 'k', 'LineWidth', 3);
        [rl2,pl2] = corrcoef(x,y);
        legend([y1_ls, y2_ls], {['Younger Adults: r=', num2str(round(rl1(2),2))], ...
            ['Older Adults: r=', num2str(round(rl2(2),2))]}, 'Location', 'NorthWest'); legend('boxoff');
        set(findall(gcf,'-property','FontSize'),'FontSize',15)
        xlabel('MPE, Scales 1:3 (>50 Hz)','FontSize',17); ylabel('PSD Slope 2-64 Hz (excl. 7-13 Hz)','FontSize',18)
        title({'Fine-scale MPE ';'positively relates to PSD slopes'},'FontSize',19)

    pn.plotFolder = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/rest/B_analyses/E_permMSE/C_figures/';
    figureName = 'D2_FineScaleAssociation';
    saveas(h, [pn.plotFolder, figureName], 'fig');
    saveas(h, [pn.plotFolder, figureName], 'epsc');
    saveas(h, [pn.plotFolder, figureName], 'png');
