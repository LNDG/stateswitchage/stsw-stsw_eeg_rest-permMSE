% Statistically compare effect sizes between different methods.

%% load MSE topography with different filter settings

pn.root = '/Users/kosciessa/Desktop/mntTardis/STSWD_Rest/WIP_eeg/E_permMSE/';
load([pn.root, 'B_data/C_CBPA_Age_Condition.mat'], 'stat', 'methodLabels')

load('/Users/kosciessa/Desktop/mntTardis/STSWD_Rest/WIP_eeg/E_permMSE/B_data/D_mpeMerged.mat')

%% get theta, alpha and beta cluster

% Note that time scales are inverted between statistical output matrix and
% original data matrices!

thetaFreqs_stat = stat{4,4}.freq>7 & stat{4,4}.freq<8;
alphaFreqs_stat = stat{4,4}.freq>10 & stat{4,4}.freq<12;
betaFreqs_stat = stat{4,4}.freq>14 & stat{4,4}.freq<20;

ThetaCluster = find(max(stat{4,4}.mask(:,thetaFreqs_stat),[],2));
AlphaCluster = find(max(stat{4,4}.mask(:,alphaFreqs_stat),[],2));
BetaCluster = find(max(stat{4,4}.mask(:,betaFreqs_stat),[],2));

thetaFreqs_data = mpeMerged{1,2}.freq>7 & mpeMerged{1,2}.freq<8;
alphaFreqs_data = mpeMerged{1,2}.freq>10 & mpeMerged{1,2}.freq<12;
betaFreqs_data = mpeMerged{1,2}.freq>14 & mpeMerged{1,2}.freq<20;

%% calculate theta age difference controlling for alpha (theta cluster)
% calculate R squared : t squared divided by (t squared +df)
% identical to correlation: corrcoef([x1;x2], [repmat(1,numel(x1),1); repmat(2,numel(x2),1)])

x1 = nanmean(nanmean(mpeMerged{1,2}.MSEbp(:,ThetaCluster,thetaFreqs_data),3),2);
x2 = nanmean(nanmean(mpeMerged{2,2}.MSEbp(:,ThetaCluster,thetaFreqs_data),3),2);
[h,p,ci,stats] = ttest2(x1,x2);
rsquare = (stats.tstat^2)./(stats.tstat^2 + stats.df);
rTheta_theta = sqrt(rsquare);

x3 = nanmean(nanmean(mpeMerged{1,2}.MSEbp(:,ThetaCluster,alphaFreqs_data),3),2);
x4 = nanmean(nanmean(mpeMerged{2,2}.MSEbp(:,ThetaCluster,alphaFreqs_data),3),2);
[h,p,ci,stats] = ttest2(x3,x4);
rsquare = (stats.tstat^2)./(stats.tstat^2 + stats.df);
rTheta_alpha = sqrt(rsquare);

xr1 = nanmean(nanmean(mpeMerged{1,2}.MSEbp(:,ThetaCluster,alphaFreqs_data),3),2);
xr2 = nanmean(nanmean(mpeMerged{2,2}.MSEbp(:,ThetaCluster,alphaFreqs_data),3),2);
[~,~,y1] = regress(x1, [repmat(1,size(xr1)), xr1]);
[~,~,y2] = regress(x2, [repmat(1,size(xr2)), xr2]);
[h,p,ci,stats] = ttest2(y1,y2);
rsquare = (stats.tstat^2)./(stats.tstat^2 + stats.df);
rTheta_alpha = sqrt(rsquare);

figure; hold on; scatter(x1, x3, 'filled'); scatter(x2, x4, 'filled')

%% calculate beta age difference controlling for alpha (beta cluster)

x1 = nanmean(nanmean(mpeMerged{1,2}.MSEbp(:,BetaCluster,betaFreqs_data),3),2);
x2 = nanmean(nanmean(mpeMerged{2,2}.MSEbp(:,BetaCluster,betaFreqs_data),3),2);
[h,p,ci,stats] = ttest2(x1,x2);
rsquare = (stats.tstat^2)./(stats.tstat^2 + stats.df);
rBeta_beta = sqrt(rsquare);

x3 = nanmean(nanmean(mpeMerged{1,2}.MSEbp(:,BetaCluster,alphaFreqs_data),3),2);
x4 = nanmean(nanmean(mpeMerged{2,2}.MSEbp(:,BetaCluster,alphaFreqs_data),3),2);
[h,p,ci,stats] = ttest2(x3,x4);
rsquare = (stats.tstat^2)./(stats.tstat^2 + stats.df);
rBeta_alpha = sqrt(rsquare);

xr1 = nanmean(nanmean(mpeMerged{1,2}.MSEbp(:,BetaCluster,alphaFreqs_data),3),2);
xr2 = nanmean(nanmean(mpeMerged{2,2}.MSEbp(:,BetaCluster,alphaFreqs_data),3),2);
[~,~,y1] = regress(x1, [repmat(1,size(xr1)), xr1]);
[~,~,y2] = regress(x2, [repmat(1,size(xr2)), xr2]);
[h,p,ci,stats] = ttest2(y1,y2);
rsquare = (stats.tstat^2)./(stats.tstat^2 + stats.df);
rBeta_alpha = sqrt(rsquare);

figure; hold on; scatter(x1, x3, 'filled'); scatter(x2, x4, 'filled')

%% calculate r-to-z transform; compare correlation coefficients

% https://www.statisticssolutions.com/comparing-correlation-coefficients/
% https://de.mathworks.com/matlabcentral/fileexchange/44658-compare_correlation_coefficients
%Zobserved = (z1-z2) / (sqrt([(1/N1-3) + (1/N2-3)]));

% The following function already performs the calculation for us:

addpath([pn.root, 'T_tools/compare_correlation_coefficients'])

compare_correlation_coefficients(rTheta_theta, rTheta_alpha, 97,97)
compare_correlation_coefficients(rBeta_beta, rBeta_alpha, 97,97)

