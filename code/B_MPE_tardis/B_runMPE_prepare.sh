#!/bin/bash

# This script prepares tardis by compiling the necessary function in MATLAB.

ssh tardis # access tardis

# check and choose matlab version
module avail matlab
module load matlab/R2016b

# compile functions

matlab
pn.rootDir = '/home/mpib/kosciessa/STSWD_Rest/WIP_eeg/E_permMSE/';
%% add fieldtrip toolbox
addpath([pn.rootDir, 'T_tools/fieldtrip-20170904/']); ft_defaults(); ft_compile_mex(true)
%% go to analysis directory containing .m-file
cd([pn.rootDir, 'A_scripts/B_MPE_tardis/'])
%% compile function and append dependencies
mcc -m B_runMPE.m -a '/home/mpib/kosciessa/STSWD_Rest/WIP_eeg/E_permMSE/T_tools/mmse/'
exit