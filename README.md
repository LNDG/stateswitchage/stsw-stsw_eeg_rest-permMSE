Scripts for the analysis of modified Multi-scale Permutation Entropy [MPE] in resting state data from younger and older adults
-
**Description of scripts**
-
A:  
- prepare resting state data for input to MPE function
- [requires empirical/mse]

B:   
- run MPE function (set to tardis cluster version)

C:
- merge data from all younger and older adults

D:
- calculate cluster-based permutation analysis (CBPA) for age contrast
- E2: plot results from CBPA
- E3: compare effect sizes for 'Original' effect clusters with different implementations

E:
- visualize PSD slopes and multi-scale sample entropy spectra
- Figure 4A
- [requires empirical/fft]

G:
- highlight association between bandpass MSE and rhythmic events
- Figure 8
- [requires empirical/ebosc_nodur]

H:
- scale-wise relation between mMSE (lowpass/band-pass) & spectral event rate
- [requires empirical/ebosc_nodur]