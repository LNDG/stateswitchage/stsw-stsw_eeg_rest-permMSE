function [pe hist] = pec(X,m,t,nrm)

%  Calculate the permutation entropy

%  Input:   X: time series;
%           m: order of permutation entropy (often 3 or 4)
%           t: delay time of permutation entropy (often 1 or 2)
%           nrm: normalization for PE (1 for yes, 2 for no)

% Output: 
%           pe:    permutation entropy
%           hist:  the histogram for the order distribution

%Ref: G Ouyang, J Li, X Liu, X Li, Dynamic Characteristics of Absence EEG Recordings with Multiscale Permutation %     %                             Entropy Analysis, Epilepsy Research, doi: 10.1016/j.eplepsyres.2012.11.003
%     X Li, G Ouyang, D Richards, Predictability analysis of absence seizures with permutation entropy, Epilepsy %     %                            Research,  Vol. 77pp. 70-74, 2007



lX = length(X);                         % amount of samples in single-channel time series
permlist = perms(1:m);                  % get all possible permutations of order m
c(1:length(permlist))=0;                % preallocate output matrix
    
 for j=1:lX-t*(m-1)                     % step through all allowed patterns
     [~,iv]=sort(X(j:t:j+t*(m-1)));     % sort in ascending order; step with step size t
     for jj=1:length(permlist)          % check for all possible patterns
         if (abs(permlist(jj,:)-iv))==0 % check whether there is a match for current pattern
             c(jj) = c(jj) + 1 ;
         end
     end
 end

hist = c;                   % save counter of how many matches were observed for each pattern
c=c(find(c~=0));            % keep only pattern that have occurred at all
p = c/sum(c);               % take ratio of matches to overall matches (normalizes to one across all patterns)
                            % should be identical to : p = c./(lX-((m-1)*t)) (e.g. see Li et al., 2013)
pe = -sum(p .* log(p));     % generate a single score of entropy across patterns

%If specified, normalize PE for total number of patterns (see Ouyang, G., Li, J., Liu, X., & Li, X. (2013). Dynamic characteristics of absence EEG recordings with multiscale permutation entropy analysis.Epilepsy Research, 104(3), 246?252.
if nrm==1
    pattern_count=numel(permlist(:,1));%gives row count, reflecting discrete patterns from "perm_patterns."
    pe = pe/log(pattern_count); % normalize such that matched amount of patterns leads to PE = 1 (Li et al., 2013)
end

% test that the permutation does what it should
% for indA = 1:10
%     x = indA/10;
%     y = (10-indA)/10;
%     p = [x,y];
%     pe = -sum(p .* log(p));
%     tmp(indA) = pe/log(2);
% end
% figure; plot(tmp)
