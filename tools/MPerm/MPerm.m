function MPE = MPerm(X,m,t,scale_low,scale_high,nrm)

%  Calculate the Multiscale Permutation Entropy (MPE) 

%  Input:   X: time series;

%           m: order of permutation entropy

%           t: delay time of permutation entropy 
%           scale_low/_high: the low and high scales you want to estimate.
%           For example, may not want the initial scales in a filtered
%           signal, as little entropy can exist there.

%           nrm: normalization for PE (1 for yes, 2 for no)

%  Output: 
%           MPE: multiscale permuation entropy

%Ref: G Ouyang, J Li, X Liu, X Li, Dynamic Characteristics of Absence EEG Recordings with Multiscale Permutation %     %                             Entropy Analysis, Epilepsy Research, doi: 10.1016/j.eplepsyres.2012.11.003
%     G Ouyang, C Dang, X Li, Complexity Analysis of EEG Data with Multiscale Permutation Entropy, Advances in %       %                      Cognitive Neurodynamics (II), 2011, pp 741-745 


MPE=[];
for j=scale_low:scale_high
    Xs = Multi(X,j);        % retrieve the scale-wise point-averaged signal
    PE = pec(Xs,m,t,nrm);   % compute permutation entropy
    MPE=[MPE PE];           % concatenate PE result across scales
end


function M_Data = Multi(Data,j)

%  generate the consecutive coarse-grained time series
%  Input:   Data: time series;
%           j: the scale of interest passed along from
%           the MPE function above (defined by scale_low and scale_high).

% Output: 
%           M_Data: the coarse-grained time series at the scale factor j.

L = length(Data);
J = fix(L/j);

for indSample=1:J
    % JQK: coarse-grain the time series by stepping averaging across increasingly large amounts of sampling points 
    M_Data(indSample) = mean(Data((indSample-1)*j+1:indSample*j));
end

%(DGarrett edited code; Oct 21, 2015)
%(JQK edited code; 02.04.2019)


%% plot influence of down-sampling:

% figure;
% for j=scale_low:scale_high
%     cla
%     Xs = Multi(X,j);
%     plot(Xs)
%     pause(.5);
% end
